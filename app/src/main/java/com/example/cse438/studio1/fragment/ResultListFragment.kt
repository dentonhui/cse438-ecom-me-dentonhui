package com.example.cse438.studio1.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.support.v7.app.AppCompatActivity
import com.example.cse438.studio1.R
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.fragment_result_list.view.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    val queryString : String = query

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        // done TODO: implement this function to inflate the fragment_result_list.xml file for the container;
        Log.d("Android:", "Incomplete")
        var v = inflater.inflate(R.layout.fragment_result_list, container, false)


        return v

    }

    override fun onStart() {
        super.onStart()
        var textView : TextView = search_for
        textView.text = "Search for: "+queryString
    }

}